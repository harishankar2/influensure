import React from "react";

const Card = (props) => {
  let category = props.category;
  let location = props.location;
  return (
    <>
      <div className="w-full md:w-6/12 lg:w-5/12 px-2 md:px-3 lg:px-4 flex flex-col">
        <div className="card shadow-lg rounded-md bg-white-btn mb-8 p-8 flex-1">
          <h5 className="text-blue-text text-2xl font-medium mb-4">
            {props.title}
          </h5>
          <p className="text-blue-text font-light text-base leading-5 mb-5">
            {props.description}
          </p>
          <div className="row text-md text-pink-btn">
            <div className="w-full flex my-2">
              <div className="w-1/2 text-left my-1">
                <i className="fa fa-user follower_no"> </i>
                <span className="text-blue-text font-bold text-md">
                  &nbsp;{props.followers}
                </span>
              </div>
              <div className="w-1/2 text-right">
                <span className="text-blue-text font-bold text-md">
                  {props.budget}&nbsp;
                </span>
                <i className="fa fa-inr budget" aria-hidden="true"></i>
              </div>
            </div>
            <div className="col-12 text-left my-1">
              <i className="fa fa-map-marker location"> </i>
              <span className="text-blue-text font-bold text-md">
                &nbsp; Locations
              </span>
              <br />
              <div className="single_values ml-3 mb-2">
                {location.map((value, ind) => {
                  return (
                    <span
                      className="text-blue-text font-bold text-md rounded border-2 border-blue-border mt-3 mr-4 py-2 px-5 bg-white-card_value inline-block"
                      key={ind}
                    >
                      {value.value}&nbsp;
                    </span>
                  );
                })}
              </div>
            </div>
            <div className="col-12 text-left my-1">
              <i className="fa fa-info category"> </i>
              <span className="text-blue-text font-bold text-md">
                &nbsp; Categories
              </span>
              <br />
              <div className="single_values ml-3 mb-2">
                {category.map((val, index) => {
                  return (
                    <span
                      className="text-blue-text font-bold text-md border-2 border-blue-border mt-5 mr-4 py-2 px-5 rounded bg-white-card_value inline-block"
                      key={index}
                    >
                      {val.value}&nbsp;
                    </span>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
/* {location.map((val, ind) => {
  return <span className="text-blue-text font-bold text-md" key={ind}>{val}&nbsp;</span>;
})} */
export default Card;
