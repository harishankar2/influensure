import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAiB1zcIg_1mJDcAmIiNt89GmPMdxt8FEA",
  authDomain: "influensure.firebaseapp.com",
  databaseURL:
    "https://influensure-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "influensure",
  storageBucket: "influensure.appspot.com",
  messagingSenderId: "984513399504",
  appId: "1:984513399504:web:1626743349166ef61eecd5",
  measurementId: "G-VCW94NT142",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
