export const Location = [
  { label: "Chennai", value: "Chennai" },
  { label: "Delhi", value: "Delhi" },
  { label: "Kolkata", value: "Kolkata" },
  { label: "Mumbai", value: "Mumbai" },
  { label: "Pune", value: "Pune" },
  { label: "Hyderabad", value: "Hyderabad" },
  { label: "Bangalore", value: "Bangalore" },
  { label: "Chandigarh", value: "Chandigarh" },
  { label: "Lucknow", value: "Lucknow" },
];

export const Category = [
  { label: "Food", value: "Food" },
  { label: "Sports", value: "Sports" },
  { label: "Education", value: "Education" },
  { label: "Science", value: "Science" },
  { label: "Music", value: "Music" },
  { label: "IT", value: "IT" },
  { label: "Dance", value: "Dance" },
  { label: "Health", value: "Health" },
  { label: "Travel", value: "Travel" },
];

export const Response = [
  { label: "< 1 Day", value: "less than 1 day" },
  { label: "< 2 Days", value: "less than 2 days" },
  { label: "< 3 Days", value: "less than 3 days" },
  { label: "< 5 Days", value: "less than 5 days" },
  { label: "< 1 Week", value: "less than 1 week" },
  { label: "< 2 Weeks", value: "less than 2 weeks" },
  { label: "< 1 Month", value: "less than 1 month" },
  { label: "> 1 Month", value: "more than 1 month" },
];

export const Brand = [
  { label: "Free", value: "Free" },
  { label: "Paid", value: "Paid" },
];
