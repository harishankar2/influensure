import { Formik, Form, ErrorMessage } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { Category, Location, Response, Brand } from "./Data";
import CustomSelect from "./CustomSelect";
import CustomSlider from "./CustomSlider";

const Campaign_Form = () => {
  const [location, setLocation] = useState([]);
  const [category, setCategory] = useState([]);
  const [response, setResponse] = useState([]);
  const [brand, setBrand] = useState([]);
  const [rangeVal, setRangeVal] = useState(40);

  const initialValues = {
    name: "",
  };

  const handleSubmit = (values) => {
    console.log(values);
  };

  const ValidationSchema = Yup.object().shape({
    name: Yup.string().required("Required!!"),
  });

  function onLocationChange(selectedLocation) {
    formik.setFieldValue("location", selectedLocation);
  }
  function onCategoryChange(selectedCategory) {
    setCategory(selectedCategory);
  }
  function onResponseChange(selectedResponse) {
    setResponse(selectedResponse);
  }
  function onBrandChange(selectedBrand) {
    setBrand(selectedBrand);
  }

  const UpdateRangeValue = (e, data) => {
    setRangeVal(data);
  };

  return (
    <>
      <div className="campaign_form_box container-fluid">
        <div className="row">
          <div className="col-11 p-5 my-5 campaign_form mx-auto m-3 align-middle">
            <div className="conatiner">
              <div className="row">
                <div className="col-12 col-lg-6">
                  <h5 className="form_title mb-4">Create Campaign</h5>
                  <Formik
                    initialValues={initialValues}
                    validationSchema={ValidationSchema}
                  >
                    <Form>
                      <div className="form-group">
                        <label htmlFor="name">Name of Campaign</label>
                        <input
                          className="my_input"
                          type="text"
                          id="name"
                          name="name"
                        />
                        <ErrorMessage
                          name="name"
                          component="div"
                          className="error"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea
                          type="text"
                          className="my_input"
                          id="description"
                          name="description"
                          maxLength="500"
                          style={{ height: "150px" }}
                        />
                        <ErrorMessage
                          name="description"
                          component="div"
                          className="error"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="location">
                          Target location of your campaign
                        </label>
                        <CustomSelect
                          id="location"
                          name="location"
                          required="required"
                          closeMenuOnSelect={false}
                          isMulti={true}
                          value={location}
                          options={Location}
                          placeholder={"Location"}
                          onChange={(selectedOption) => {
                            onLocationChange(selectedOption);
                          }}
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="category">
                          Category(s) for your campaign
                        </label>
                        <CustomSelect
                          id="category"
                          name="category"
                          closeMenuOnSelect={false}
                          required="required"
                          isMulti={true}
                          value={category}
                          options={Category}
                          placeholder={"Category"}
                          onChange={(selectedOption) => {
                            onCategoryChange(selectedOption);
                          }}
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="slider_input">No of Followers</label>
                        <CustomSlider
                          value={rangeVal}
                          onChange={UpdateRangeValue}
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="response">
                          Average response time of the Influencers
                        </label>
                        <CustomSelect
                          id="response"
                          name="response"
                          required="required"
                          isMulti={false}
                          closeMenuOnSelect={true}
                          options={Response}
                          value={response}
                          placeholder={"Response"}
                          onChange={(selectedOption) => {
                            onResponseChange(selectedOption);
                          }}
                          theme={(theme) => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "hotpink",
                              primary: "black",
                            },
                          })}
                        />
                      </div>
                      <div className="form-group">
                        <label>What is your budget for this campaign?</label>
                        <div className="row">
                          <div className="col-4">
                            <input
                              className="my_input"
                              type="number"
                              id="budget_min"
                              name="budget_min"
                              placeholder="Min"
                            />
                            <ErrorMessage
                              name="budget_min"
                              component="div"
                              className="error"
                            />
                          </div>
                          <div className="col-4">
                            <input
                              className="my_input"
                              type="number"
                              id="budget_max"
                              name="budget_max"
                              placeholder="Max"
                            />
                            <ErrorMessage
                              name="budget_max"
                              component="div"
                              className="error"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="rate">
                          Post Engagement rate of the Influencers
                        </label>
                        <input
                          className="my_input"
                          type="percentage"
                          id="rate"
                          name="rate"
                        />
                        <ErrorMessage
                          name="rate"
                          component="div"
                          className="error"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="brand_kind">Kind of brand collab</label>
                        <CustomSelect
                          id="brand_kind"
                          name="brand_kind"
                          required="required"
                          isMulti={false}
                          options={Brand}
                          value={brand}
                          placeholder={"Kind of Brand"}
                          onChange={(selectedOption) => {
                            onBrandChange(selectedOption);
                          }}
                          theme={(theme) => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                              ...theme.colors,
                              primary25: "hotpink",
                              primary: "black",
                            },
                          })}
                        />
                      </div>
                      <div className="form-group text-left">
                        <label>Have you worked with any brand?</label>
                        <div className="radio_btn">
                          <input type="radio" id="option1" name="question" />
                          <label htmlFor="option1"> Yes </label>
                          <input type="radio" id="option2" name="question" />
                          <label htmlFor="option2"> No </label>
                        </div>

                        <ErrorMessage
                          name="question"
                          component="div"
                          className="error"
                        />
                      </div>
                      <button
                        className="create_campaign"
                        type="submit"
                        onClick={handleSubmit}
                      >
                        Create
                      </button>
                    </Form>
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Campaign_Form;
