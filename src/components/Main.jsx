import React, { useContext, useEffect, useState } from "react";
import Card from "./Card";
import { FilterContext } from "./FilterContext";
import empty from "../img/empty.svg";
import nodata from "../img/no_data.svg";
import db from "./firebase";

const Main = () => {
  const { filter, setFilter } = useContext(FilterContext);
  const [campaigns, setCampaigns] = useState([]);

  useEffect(() => {
    const unsubscribe = db
      .collection("data")
      .onSnapshot((snapshot) =>
        setCampaigns(snapshot.docs.map((doc) => doc.data()))
      );

    return () => {
      unsubscribe();
    };
  }, []);

  let filtered_campaigns = campaigns
    .filter(
      (campaign) =>
        campaign.followers >= filter.followers ||
        filter.followers === "" ||
        filter.followers === "Select"
    )
    .filter(
      (campaign) =>
        campaign.budget_min >= filter.budget_min || filter.budget_min === ""
    )
    .filter(
      (campaign) =>
        campaign.budget_max <= filter.budget_max || filter.budget_max === ""
    )
    .filter((campaign) => {
      let location_filter = false;
      campaign.location.map((single_loc) => {
        if (single_loc.value === filter.location || filter.location === "") {
          location_filter = true;
        }
        return location_filter;
      });
      return location_filter;
    })
    .filter((campaign) => {
      let category_filter = false;
      campaign.category.map((single_cat) => {
        if (
          single_cat.value === filter.category ||
          filter.category === "" ||
          filter.category === "Select"
        ) {
          category_filter = true;
        }
        return category_filter;
      });
      return category_filter;
    });

  return (
    <>
      <div className="flex flex-wrap bg-gray-bg p-2 pt-12 lg:px-12 md:px-6 px-1">
        {filtered_campaigns.length > 0 ? (
          filtered_campaigns.map((val, ind) => {
            let budget = val.budget_min + " - " + val.budget_max;
            return (
              <Card
                key={ind}
                category={val.category}
                location={val.location}
                title={val.name}
                description={val.description}
                followers={val.followers}
                budget={budget}
              />
            );
          })
        ) : (
          <div className="mt-10 p-5 h-full w-full pb-40">
            <div className="result mx-auto">
              {campaigns.length > 0 ? (
                <img
                  src={nodata}
                  alt=""
                  style={{
                    height: "auto",
                    width: "100%",
                    maxHeight: "300px",
                  }}
                />
              ) : (
                <img
                  src={empty}
                  alt=""
                  style={{
                    height: "auto",
                    width: "100%",
                    maxHeight: "300px",
                  }}
                />
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Main;

/* 
.filter((campaign) => {
  let category_filter = false;
  campaign.category.map((single_cat) => {
    if (single_cat.value === filter.category) {
      category_filter = true;
    }
    return category_filter;
  });
  return category_filter;
}) */

/* 

.filter((campaign) => {
  let location_filter = false;
  campaign.location.map((single_loc) => {
    if (single_loc.value === filter.location) {
      location_filter = true;
    }
    return location_filter;
  });
  return location_filter;
}) */
