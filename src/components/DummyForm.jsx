import React, { useState } from "react";
import Select from "react-select";

const DummyForm = () => {
  const [name, setName] = useState("");
  const [min, setMin] = useState("");
  const [max, setMax] = useState("");
  const [selectedYear, setSelectedYear] = useState("");

  const yearOptions = [
    { value: "1960", label: "1960" },
    { value: "1961", label: "1961" },
    { value: "1962", label: "1962" },
    { value: "1963", label: "1963" },
    { value: "1964", label: "1964" },
    { value: "1965", label: "1965" },
  ];

  const handleYearChange = (selectedYear) => {
    setSelectedYear(selectedYear);
  };

  const DummySubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
    if (isValid) {
      console.log(name, min, max);
    }
  };

  const formValidation = () => {
    let isValid = true;
    if (name.trim().length < 1) {
      document.getElementById("name_err").innerHTML = "Required";
      isValid = false;
    } else if (!/^[a-zA-Z]*$/g.test(name)) {
      document.getElementById("name_err").innerHTML =
        "Name should contain only alphabets";
      isValid = false;
    } else if (name.trim().length < 3) {
      document.getElementById("name_err").innerHTML =
        "Name should have at least 3 characters";
      isValid = false;
    } else if (name.trim().length > 15) {
      document.getElementById("name_err").innerHTML =
        "Name should not exceed 15 characters";
      isValid = false;
    } else {
      document.getElementById("name_err").innerHTML = null;
    }

    if (min.length < 1 || max.length < 1) {
      document.getElementById("min_max_error").innerHTML = "Required";
      isValid = false;
    } else if (max <= min) {
      document.getElementById("min_max_error").innerHTML =
        "Max value should be greater than min value";
      isValid = false;
    } else {
      document.getElementById("min_max_error").innerHTML = null;
    }

    if (selectedYear.length === 0) {
      document.getElementById("error_year").innerHTML = "Select Year";
    } else {
      document.getElementById("error_year").innerHTML = null;
    }

    return isValid;
  };

  return (
    <div className="dummyform">
      <h1>Hello</h1>
      <form onSubmit={DummySubmit}>
        <label htmlFor="name">Name</label>
        <input
          className="form-control"
          autoComplete="off"
          type="text"
          placeholder="Name"
          id="name"
          name="name"
          value={name}
          onChange={(event) => setName(event.target.value)}
        />
        <div style={{ color: "red" }} id="name_err"></div>
        <div className="row mt-2">
          <div className="col-6">
            <label htmlFor="min">Min</label>
            <input
              className="form-control"
              autoComplete="off"
              type="number"
              placeholder="Min"
              id="min"
              name="min"
              value={min}
              onChange={(event) => setMin(parseInt(event.target.value, 10))}
            />
          </div>
          <div className="col-6">
            <label htmlFor="max">Max</label>
            <input
              className="form-control"
              autoComplete="off"
              type="number"
              placeholder="Max"
              id="max"
              name="max"
              value={max}
              onChange={(event) => setMax(parseInt(event.target.value, 10))}
            />
          </div>
        </div>
        <div style={{ color: "red" }} id="min_max_error"></div>

        <div className="mt-2">
          <Select
            placeholder="Year"
            value={selectedYear}
            onChange={(selectedOption) => {
              handleYearChange(selectedOption);
            }}
            isSearchable={true}
            options={yearOptions}
            name="year"
            isMulti={true}
          />
          <div style={{ color: "red" }} id="error_year"></div>
        </div>

        <button type="submit" className="btn btn-primary mt-3 form-control">
          Submit
        </button>
      </form>
    </div>
  );
};

export default DummyForm;
