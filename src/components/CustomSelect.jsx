import React from "react";
import Select from "react-select";

const colourStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: "white",
    padding: "6px 2px",
    border: "solid 1px #b6cbff",
    marginTop: "10px",
  }),
  option: (styles, { isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
        ? "blue"
        : isFocused
        ? "pink"
        : null,

      ":active": {
        ...styles[":active"],
        backgroundColor: !isDisabled && (isSelected ? "blue" : "pink"),
      },
    };
  },
  multiValue: (styles) => {
    return {
      ...styles,
      backgroundColor: "#addeff",
      border: "solid 0.6px #8cc8ff",
    };
  },
  multiValueLabel: (styles) => ({
    ...styles,
    color: "#201d5e",
    fontWeight: "bold",
    padding: "4px 14px",
    borderRadius: "3px",
  }),
  multiValueRemove: (styles) => ({
    ...styles,
    color: "#201d5e",
    ":hover": {
      backgroundColor: "red",
      color: "white",
    },
  }),
};

const CustomSelect = ({
  id,
  name,
  placeholder,
  options,
  isMulti,
  isSearchable,
  closeMenuOnSelect,
  onChange,
}) => (
  <Select
    id={id}
    name={name}
    isSearchable={isSearchable}
    placeholder={placeholder}
    closeMenuOnSelect={closeMenuOnSelect}
    isMulti={isMulti}
    options={options}
    styles={colourStyles}
    onChange={onChange}
  />
);

export default CustomSelect;
