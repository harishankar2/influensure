import React from "react";
import { Link } from "@reach/router";
import fb from "../img/fb.svg";
import insta from "../img/insta.svg";
import linkedin from "../img/linkedin.svg";

const Footer = () => {
  return (
    <div className="pt-20 pb-12 bg-gray-bg">
      <div className="row">
        <div className="text-center font-semibold text-xl text-blue-text my-6">
          <Link to="/" className="navbar-brand">
            Influensure
          </Link>
        </div>
        <div className="col-12 flex justify-center items-center my-6">
          <Link className="mx-3" to="/">
            <img src={fb} style={{ height: "20px" }} alt="" />
          </Link>
          <Link className="mx-3" to="/">
            <img src={insta} style={{ height: "20px" }} alt="" />
          </Link>
          <Link className="mx-3" to="/">
            <img src={linkedin} style={{ height: "20px" }} alt="" />
          </Link>
        </div>
        <div className="col-12 mt-3 text-center">
          <Link className="mx-4 text-sm font-medium text-blue-text" to="/">
            About
          </Link>
          <Link className="mx-4 text-sm font-medium text-blue-text" to="/">
            Solutions
          </Link>
          <Link className="mx-4 text-sm font-medium text-blue-text" to="/">
            Blog
          </Link>
          <Link className="mx-4 text-sm font-medium text-blue-text" to="/">
            Pricing
          </Link>
        </div>
        <div className="col-12 mt-3 text-center text-lg">
          <p className="text-sm font-medium text-blue-text">
            &#169; 2020 Influensure
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
