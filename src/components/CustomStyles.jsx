export const CustomStyles = {
  control: (styles) => ({ ...styles, backgroundColor: "white" }),
  option: (styles, { isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
        ? "blue"
        : isFocused
        ? "pink"
        : null,
      color: isDisabled ? "#ccc" : isSelected ? "white" : "black",
      cursor: isDisabled ? "not-allowed" : "default",

      ":active": {
        ...styles[":active"],
        backgroundColor: !isDisabled && (isSelected ? "blue" : "pink"),
      },
    };
  },
  multiValue: (styles) => {
    return {
      ...styles,
      backgroundColor: "skyblue".aplha(0.1).css(),
    };
  },
  multiValueLabel: (styles) => ({
    ...styles,
    color: "blue",
  }),
  multiValueRemove: (styles) => ({
    ...styles,
    color: "blue",
    ":hover": {
      backgroundColor: "red",
      color: "white",
    },
  }),
};
