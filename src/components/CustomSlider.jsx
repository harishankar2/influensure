import React from "react";
import { withStyles, makeStyles, createStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";

const useStyles = makeStyles((theme) =>
  createStyles({
    margin: {
      height: theme.spacing(1),
    },
  })
);

const PrettoSlider = withStyles({
  root: {
    color: "#ff8896",
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: "#201d5e",
    marginTop: -8,
    marginLeft: -12,
    "&:focus, &:hover, &$active": {
      boxShadow: "inherit",
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 7px)",
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
    backgroundColor: "#e2e2e2",
  },
})(Slider);

const CustomSlider = (props) => {
  const classes = useStyles();
  const { defaultSliderValue, onChange, min, max, step } = props;

  return (
    <div className={classes.root}>
      <div className={classes.margin} />
      <PrettoSlider
        value={defaultSliderValue}
        min={min}
        max={max}
        step={step}
        valueLabelDisplay="auto"
        aria-label="pretto slider"
        onChange={onChange}
      />
    </div>
  );
};

export default CustomSlider;
