import React, { useContext, useState } from "react";
import { Link, navigate } from "@reach/router";
import { LoginContext } from "./LoginContext";

const Navbar = () => {
  const [navbarOpen, setNavbarOpen] = useState(false);
  const { signIn, setSignIn } = useContext(LoginContext);
  let login_data = JSON.parse(localStorage.getItem("login"));

  return (
    <>
      <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg bg-white-btn">
        <div className="px-2 container mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              className="font-semibold text-xl text-blue-text leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap"
              to="/"
            >
              Influensure
            </Link>
            <button
              className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none "
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}
            >
              <i className="fa fa-bars "></i>
            </button>
          </div>
          <div
            className={
              "lg:flex flex-grow items-center " +
              (navbarOpen ? " flex" : " hidden")
            }
            id="example-navbar-danger"
          >
            <ul className="flex flex-col lg:flex-row w-full list-none lg:mr-auto">
              <li className="nav-item">
                <a
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  href="#pablo"
                >
                  <span className="ml-2 text-blue-text">About us</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  href="#pablo"
                >
                  <span className="ml-2 text-blue-text">How it works</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  href="#pablo"
                >
                  <span className="ml-2 text-blue-text">Solutions</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  href="#pablo"
                >
                  <span className="ml-2 text-blue-text">Blog</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  href="#pablo"
                >
                  <span className="ml-2 text-blue-text">Pricing</span>
                </a>
              </li>
              <li className="nav-item lg:ml-auto">
                <Link
                  className="px-3 py-2 flex items-center text-sm mt-4 font-semibold leading-snug text-white hover:opacity-75"
                  to="/"
                  onClick={() => {
                    localStorage.removeItem("login");
                    setSignIn(false);
                  }}
                >
                  <span className="ml-2 text-blue-text">Log Out</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/campaign"
                  className="px-3 py-2 flex items-center text-sm font-semibold leading-snug text-white hover:opacity-75"
                >
                  <span className="ml-2 text-white-btn campaign_button">
                    Create Campaign
                  </span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="flex justify-center bg-gray-bg py-2 items-center">
        <span className="text-2xl text-blue-text font-medium">
          Welcome {login_data ? login_data._profile.name : null}
        </span>
      </div>
    </>
  );
};

export default Navbar;
