import React from "react";
import { Link } from "@reach/router";
import "./header.css";

const Header = () => {
  return (
    <>
      <div className="header">
        <div className="row">
          <div className="col-11 mx-auto m-3 align-middle">
            {/* <Link className="navbar-brand" to="/">
              Influensure
            </Link>
            <Link to="/campaign" className="btn create_campaign float-right">
              Create Campaign
            </Link> */}
            <nav className="navbar navbar-expand-lg navbar-light">
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <Link to="/" className="navbar-brand">
                Influensure
              </Link>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav">
                  <li className="nav-item ">
                    <Link className="nav-link" to="/">
                      About us
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/">
                      How it works
                    </Link>
                  </li>

                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      to="#"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Solutions
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <Link className="dropdown-item" to="/">
                        Option 1
                      </Link>
                      <Link className="dropdown-item" to="/">
                        Option 2
                      </Link>
                      <Link className="dropdown-item" to="/">
                        Option 3
                      </Link>
                    </div>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link " to="/">
                      Blog
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/">
                      Pricing
                    </Link>
                  </li>
                </ul>
                <Link className="login_link ml-auto" to="/">
                  Login
                </Link>

                <Link
                  to="/campaign"
                  className="nav-link btn campaign_button text-white-btn"
                >
                  Create Campaign
                </Link>
              </div>
              {/*               <div className="float-right">
                <Link to="/" className="login nav-item mr-5">
                  Login
                </Link>
                <Link
                  to="/campaign"
                  className="btn create_campaign float-right"
                >
                  Create Campaign
                </Link>
              </div> */}
            </nav>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
