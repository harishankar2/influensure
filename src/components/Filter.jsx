import React from "react";
import "./filter.css";

const Filter = () => {
  return (
    <div className="py-6 border-red-300 bg-gray-bg">
      <div className="row bg-white-btn">
        <div className="col-11 mx-auto py-3 px-4">
          <div className="row px-3 md:flex">
            <div className="w-full md:w-4/12">
              <div className="form-group">
                <label className="my_label" htmlFor="location">
                  Location
                </label>
                <input
                  type="text"
                  className="my_input border-blue-border"
                  id="location"
                  placeholder="Search Location"
                />
              </div>
            </div>
            <div className="w-full md:2/12">
              <label className="my_label" htmlFor="followers">
                No of followers
              </label>
              <select
                id="followers"
                name="followers"
                className="custom-select block border-blue-border_select  text-blue-text font-medium"
              >
                <option>5,000</option>
                <option>10,000</option>
                <option selected>15,000</option>
                <option>20,000</option>
                <option>25,000</option>
                <option>30,000</option>
                <option>40,000</option>
                <option>50,000</option>
              </select>
            </div>
            <div className="w-full md:w-4/12">
              <div className="col-12">
                <label className="my_label" htmlFor="budget_min">
                  Location
                </label>
              </div>
              <div className="flex">
                <input
                  type="number"
                  className="w-1/2 border-blue-border"
                  id="budget_min"
                  placeholder="Min"
                />
                <input
                  type="number"
                  className="w-1/2 "
                  id="budget_max"
                  placeholder="Max"
                />
              </div>

              <div className="w-full md:w-2/12">
                <label className="my_label" htmlFor="category">
                  Categories
                </label>
                <select
                  id="category"
                  name="category"
                  className="custom-select border-blue-border_select  text-blue-text font-medium"
                >
                  <option>Select</option>
                  <option>Fashion</option>
                  <option>Food</option>
                  <option>Travel</option>
                  <option>Education</option>
                </select>
              </div>
            </div>
            <div className="col-12 mt-8 mb-4 text-center">
              <h6 className="text-blue-text underline">More Filters</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Filter;
