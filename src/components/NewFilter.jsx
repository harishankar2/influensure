import React, { useContext, useState } from "react";
import { useFormik } from "formik";
import { FilterContext } from "./FilterContext";

const NewFilter = () => {
  const { filter, setFilter } = useContext(FilterContext);
  const [filterOpen, setFilterOpen] = useState(false);

  const initialValues = filter;
  localStorage.setItem("filter", JSON.stringify(initialValues));

  const onSubmit = (values) => {
    if (
      values.location !== "" ||
      values.followers !== "" ||
      values.budget_min !== "" ||
      values.budget_min !== "" ||
      values.category !== ""
    ) {
      setFilter(values);
    }
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: true,
  });

  return (
    <>
      <button
        onClick={() => setFilterOpen(!filterOpen)}
        className="sm:hidden mb-3 mt-2 rounded float-right mr-4 font-medium bg-white-btn text-blue-text py-1 px-7 border hover:opacity-75 border-pink-btn"
      >
        FILTER
      </button>
      <div className="bg-gray-bg py-10 ">
        <div
          className={
            "bg-white-btn mt-3 pb-6 sm:block " +
            (filterOpen ? "block" : "hidden")
          }
        >
          <form onSubmit={formik.handleSubmit}>
            <div className="filter_content md:flex w-full px-2 md:px-8 pb-6 pt-2">
              <div className="px-4 mt-6 md:mt-2 w-full md:w-3/12 ">
                <label className="my_label" htmlFor="location">
                  Location
                </label>
                <input
                  type="text"
                  className="my_input"
                  id="location"
                  name="location"
                  placeholder="Search Location"
                  {...formik.getFieldProps("location")}
                />
              </div>
              <div className="px-4 mt-6 md:mt-2 w-full md:w-3/12 ">
                <label className="my_label" htmlFor="followers">
                  No of followers
                </label>
                <select
                  id="followers"
                  name="followers"
                  {...formik.getFieldProps("followers")}
                  className="my_input text-blue-text"
                >

                  <option>Select</option>
                  <option>5000</option>
                  <option>10000</option>
                  <option>15000</option>
                  <option>20000</option>
                  <option>30000</option>
                  <option>40000</option>
                  <option>50000</option>
                  <option>60000</option>
                  <option>70000</option>
                  <option>80000</option>
                  <option>90000</option>
                  <option>100000</option>
                </select>
              </div>
              <div className="input_field mt-6 md:mt-2 w-full md:w-4/12 px-4">
                <label className="my_label" htmlFor="location">
                  Budget
                </label>
                <div className="budget_input sm:flex">
                  <div className="w-full sm:mt-0 mt-2 sm:w-1/2 sm:pr-3">
                    <input
                      type="number"
                      className=" my_input"
                      id="budget_min"
                      name="budget_min"
                      placeholder="Min"
                      {...formik.getFieldProps("budget_min")}
                    />
                  </div>
                  <div className="w-full sm:mt-0 mt-2 sm:w-1/2 sm:pl-3">
                    <input
                      type="number"
                      className=" my_input"
                      id="budget_max"
                      name="budget_max"
                      placeholder="Max"
                      {...formik.getFieldProps("budget_max")}
                    />
                  </div>
                </div>
              </div>
              <div className="px-4 mt-6 md:mt-2 w-full md:w-2/12 ">
                <label className="my_label" htmlFor="category">
                  Categories
                </label>
                <select
                  id="category"
                  name="category"
                  className="my_input text-blue-text "
                  {...formik.getFieldProps("category")}
                >
                  <option>Select</option>
                  <option>Sports</option>
                  <option>Food</option>
                  <option>Science</option>
                  <option>Education</option>
                  <option>Music</option>
                  <option>IT</option>
                  <option>Dance</option>
                  <option>Health</option>
                  <option>Travel</option>
                </select>
              </div>
            </div>
            <div className="mt-6 text-center ">
              <button
                type="submit"
                className="text-blue-text font-medium rounded py-2 px-4 bg-gray-bg border border-pink-btn"
              >
                Filter Now
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default NewFilter;
