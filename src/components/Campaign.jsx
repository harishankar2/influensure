import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Location, Category, Response, Brand } from "./Data";
import CustomSelect from "./CustomSelect";
import CustomSlider from "./CustomSlider";
import { navigate } from "@reach/router";
import db from "./firebase";

const Campaign = () => {
  const initialValues = {
    name: "",
    description: "",
    location: [],
    category: [],
    response: [],
    followers: 20000,
    budget_min: "",
    budget_max: "",
    rate: "",
    brand: [],
    question: "",
  };

  const onSubmit = (values) => {
    console.log(values);
    db.collection("data")
      .doc(`${values.name}`)
      .set(values)
      .then(function () {
        console.log("Document successfully written!");
      })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      });
    /*     let new_data = values;
    if (localStorage.getItem("campaignList") == null) {
      localStorage.setItem("campaignList", "[]");
    }
    let old_data = JSON.parse(localStorage.getItem("campaignList"));
    old_data.push(new_data);

    localStorage.setItem("campaignList", JSON.stringify(old_data)); */
    navigate("/");
  };

  const validationSchema = Yup.object({
    name: Yup.string()
      .min(3, "Name should have at least 3 characters")
      .max(30, "Name should not exceed 30 characters")
      .required("Required"),
    description: Yup.string()
      .trim()
      .min(10, "Description should have at least 10 characters")
      .required("Required!"),
    location: Yup.array()
      .nullable()
      .min(1, "Please select at least one value")
      .max(3, "Please select maximum of 3 tags only")
      .of(
        Yup.object().shape({
          value: Yup.string().required(),
          label: Yup.string().required(),
        })
      ),
    category: Yup.array()
      .nullable()
      .min(1, "Please select at least one value")
      .max(3, "Please select maximum of 3 tags only")
      .of(
        Yup.object().shape({
          label: Yup.string().required(),
          value: Yup.string().required(),
        })
      ),
    response: Yup.array()
      .nullable()
      .min(1, "Please select at least one value")
      .of(
        Yup.object().shape({
          label: Yup.string().required(),
          value: Yup.string().required(),
        })
      ),
    budget_min: Yup.number()
      .positive()
      .lessThan(Yup.ref("budget_max"), "Should be less than max Budget")
      .integer()
      .required("Enter Min Budget Value"),
    budget_max: Yup.number()
      .positive()
      .moreThan(Yup.ref("budget_min"), "Should be more than min Budget")
      .integer()
      .required("Enter Max Budget Value"),
    rate: Yup.number()
      .required("Please enter the rate % of influencers")
      .min(1, "Percentaeg should be at least 1 %")
      .max(100, "Percentage should be less than 100")
      .positive()
      .integer(),
    brand: Yup.array()
      .nullable()
      .min(1, "Please select at least one value")
      .of(
        Yup.object().shape({
          label: Yup.string().required(),
          value: Yup.string().required(),
        })
      ),
    question: Yup.string().required("Required"),
  });

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: true,
  });

  function onLocationChange(selectedLocation) {
    formik.setFieldValue("location", selectedLocation);
  }
  function onCategoryChange(selectedCategory) {
    formik.setFieldValue("category", selectedCategory);
  }
  function onResponseChange(selectedResponse) {
    formik.setFieldValue("response", selectedResponse);
  }
  function onBrandChange(selectedBrand) {
    formik.setFieldValue("brand", selectedBrand);
  }

  function onFolChange(event, newValue) {
    formik.setFieldValue("followers", newValue);
  }

  return (
    <>
      <div className="bg-gray-bg py-10 px-2 sm:px-8 md:px-12">
        <div className="p-2 md:p-8 my-5 bg-white-btn py-10 rounded-lg mx-auto m-2 align-middle">
          <div className="conatiner">
            <div className="row">
              <div className="col-12 col-lg-6">
                <h5 className="form_title text-3xl text-blue-text font-bold mb-8">
                  Create Campaign
                </h5>
                <form onSubmit={formik.handleSubmit}>
                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="name">
                      Name of Campaign
                    </label>
                    <input
                      className="my_input "
                      type="text"
                      id="name"
                      name="name"
                      placeholder="Name"
                      {...formik.getFieldProps("name")}
                    />
                    {formik.touched.name && formik.errors.name ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.name}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="description">
                      Description
                    </label>
                    <textarea
                      type="text"
                      className="my_input h-20"
                      id="description"
                      name="description"
                      placeholder="Write Here..."
                      maxLength="500"
                      {...formik.getFieldProps("description")}
                      style={{ height: "150px" }}
                    />
                    {formik.touched.description && formik.errors.description ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.description}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="location">
                      Target location of your campaign
                    </label>
                    <CustomSelect
                      id="location"
                      name="location"
                      placeholder={"Location"}
                      required="required"
                      closeMenuOnSelect={false}
                      isMulti={true}
                      isSearchable={true}
                      options={Location}
                      onChange={(selectedOption) => {
                        onLocationChange(selectedOption);
                      }}
                    />
                    {formik.touched.location && formik.errors.location ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.location}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="category">
                      Category(s) for your campaign
                    </label>
                    <CustomSelect
                      id="category"
                      name="category"
                      closeMenuOnSelect={false}
                      required="required"
                      isMulti={true}
                      isSearchable={true}
                      options={Category}
                      placeholder={"Category"}
                      onChange={(selectedOption) => {
                        onCategoryChange(selectedOption);
                      }}
                    />
                    {formik.touched.category && formik.errors.category ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.category}
                      </div>
                    ) : null}
                  </div>
                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="followers">
                      No of followers
                    </label>
                    <CustomSlider
                      id="followers"
                      name="followers"
                      min={0}
                      step={500}
                      max={100000}
                      defaultSliderValue={formik.values.followers}
                      onChange={onFolChange}
                      // value={formik.values.followers}
                    />
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="response">
                      Average response time of the Influencers
                    </label>
                    <CustomSelect
                      id="response"
                      name="response"
                      required="required"
                      isMulti={false}
                      closeMenuOnSelect={true}
                      options={Response}
                      placeholder={"Response"}
                      theme={(theme) => ({
                        ...theme,
                        borderRadius: 0,
                        colors: {
                          ...theme.colors,
                          primary25: "hotpink",
                          primary: "black",
                        },
                      })}
                      onChange={(selectedOption) => {
                        onResponseChange(selectedOption);
                      }}
                    />
                    {formik.touched.response && formik.errors.response ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.response}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label">
                      What is your budget for this campaign?
                    </label>
                    <div className="flex w-full">
                      <div className="w-1/2 pr-3">
                        <input
                          className="my_input"
                          type="number"
                          id="budget_min"
                          name="budget_min"
                          placeholder="Min"
                          {...formik.getFieldProps("budget_min")}
                        />
                        {formik.touched.budget_min &&
                        formik.errors.budget_min ? (
                          <div className="text-red-700 font-medium error">
                            {formik.errors.budget_min}
                          </div>
                        ) : null}
                      </div>

                      <div className="w-1/2 pl-3">
                        <input
                          className="my_input"
                          type="number"
                          id="budget_max"
                          name="budget_max"
                          placeholder="Max"
                          {...formik.getFieldProps("budget_max")}
                        />
                        {formik.touched.budget_max &&
                        formik.errors.budget_max ? (
                          <div className="text-red-700 font-medium error">
                            {formik.errors.budget_max}
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="rate">
                      Post Engagement rate of the Influencers
                    </label>
                    <input
                      className="my_input"
                      type="number"
                      placeholder="Rate % of influencers"
                      id="rate"
                      name="rate"
                      {...formik.getFieldProps("rate")}
                    />
                    {formik.touched.rate && formik.errors.rate ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.rate}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6">
                    <label className="my_label" htmlFor="brand">
                      Kind of brand collab
                    </label>
                    <CustomSelect
                      id="brand"
                      name="brand"
                      required="required"
                      isSearchable={false}
                      isMulti={false}
                      closeMenuOnSelect={true}
                      options={Brand}
                      placeholder="Kind of Brand"
                      theme={(theme) => ({
                        ...theme,
                        borderRadius: 0,
                        colors: {
                          ...theme.colors,
                          primary25: "hotpink",
                          primary: "black",
                        },
                      })}
                      onChange={(selectedOption) => {
                        onBrandChange(selectedOption);
                      }}
                    />
                    {formik.touched.brand && formik.errors.brand ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.brand}
                      </div>
                    ) : null}
                  </div>

                  <div className=" w-full md:w-7/12 mb-6 text-left">
                    <label className="my_label">
                      Have you worked with any brand?
                    </label>
                    <div className="flex w-32 pl-2">
                      <input
                        className="mt-0.5 mr-1"
                        type="radio"
                        id="option1"
                        name="question"
                        value="Yes"
                        onChange={formik.handleChange}
                      />
                      <label className="my_label -mt-1  mr-4" htmlFor="option1">
                        Yes
                      </label>
                      <input
                        className="mt-0.5 mr-1"
                        type="radio"
                        id="option2"
                        name="question"
                        value="No"
                        onChange={formik.handleChange}
                      />
                      <label className="my_label -mt-1" htmlFor="option2">
                        No
                      </label>
                    </div>
                    {formik.touched.question && formik.errors.question ? (
                      <div className="text-red-700 font-medium error">
                        {formik.errors.question}
                      </div>
                    ) : null}
                  </div>

                  <button
                    className="campaign_button text-white-btn hover:opacity-75"
                    type="submit"
                  >
                    Create
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Campaign;
