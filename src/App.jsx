import React, { useState } from "react";
import Home from "./pages/Home";
import Campaign from "./pages/Create_Campaign";
import { Router } from "@reach/router";
/* import firebase from "firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth"; */
import Signin from "./pages/Signin";
import { LoginContext } from "./components/LoginContext";

const App = () => {
  const [signIn, setSignIn] = useState(false);
  let old_data = JSON.parse(localStorage.getItem("login"));

  return (
    <>
      <LoginContext.Provider value={{ signIn, setSignIn }}>
        <Router>
          {signIn || old_data ? (
            <>
              <Home path="/" />
              <Campaign path="/campaign" />
            </>
          ) : (
            <Signin path="/" />
          )}
        </Router>
      </LoginContext.Provider>
    </>
  );
};

export default App;

/* import React, { useState } from "react";
/* import Home from "./pages/Home";
import Campaign from "./pages/Create_Campaign"; 
import Index from "./pages/Index";
import About from "./pages/About";
import { Router, Link } from "@reach/router";
import { UserContext } from "./pages/UserContext";

const App = () => {
  const [value, setValue] = useState("Hello");
  return (
    <>
      <ul>
        <li>
          <Link to="/">Index</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>

      {/*         <Home path="/" />
        <Campaign path="/campaign" /> }

      <UserContext.Provider value={{ value, setValue }yyy}>
        <Router>
          <Index path="/" />
          <About path="/about" />
        </Router>
      </UserContext.Provider>
    </>
  );
};

export default App;
 */
