import React, { useState } from "react";
import NewFilter from "../components/NewFilter";
import Main from "../components/Main";
import Navbar from "../components/Navbar";
import { FilterContext } from "../components/FilterContext";

const Home = () => {
  const [filter, setFilter] = useState({
    location: "",
    followers: "",
    budget_min: "",
    budget_max: "",
    category: "",
  });
  return (
    <>
      <Navbar />
      <FilterContext.Provider value={{ filter, setFilter }}>
        <NewFilter />
        <Main />
      </FilterContext.Provider>
    </>
  );
};

export default Home;
