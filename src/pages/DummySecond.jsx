import React, { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import CustomSelect from "../components/CustomSelect";
import Select from "react-select";

const DummySecond = () => {
  const [selectedYear, setSelectedYear] = useState(null);

  const testSchema = Yup.object().shape({
    name: Yup.string().required("Enter Name"),
    year: Yup.string().required("Select Year"),
  });

  const initialValues = {
    name: "",
    year: "",
  };

  /*   const handleYearChange = (selectedYear) => {
    console.log(selectedYear);
    setSelectedYear(selectedYear);
  }; */

  const yearOptions = [
    { value: "1960", label: "1960" },
    { value: "1961", label: "1961" },
    { value: "1962", label: "1962" },
    { value: "1963", label: "1963" },
    { value: "1964", label: "1964" },
    { value: "1965", label: "1965" },
  ];

  return (
    <Formik validationSchema={testSchema} initialValues={initialValues}>
      {({
        handleChange,
        handleBlur,
        values,
        errors,
        touched,
        handleSubmit,
        setFieldTouched,
      }) => {
        return (
          <>
            <div className="container mt-5">
              <div className="col-8 mx-auto">
                <input
                  label="Name"
                  name="name"
                  margin="normal"
                  variant="outlined"
                  className="form-control"
                  onChange={handleChange("name")}
                  style={{ width: "100%", zIndex: 0 }}
                  value={values.name}
                  onBlur={() => {
                    console.log("name");
                  }}
                />
                {errors.name}
              </div>

              <div className="col-8 mx-auto">
                <Select
                  name="year"
                  placeholder="Year"
                  value={selectedYear}
                  onChange={setSelectedYear}
                  options={yearOptions}
                  isMulti={true}
                  isSearchable
                  autoFocus
                  noOptionsMessage={() => "No other year"}
                />
                {selectedYear === null ? errors.year : null}
              </div>
              <div className="col-2 mx-auto">
                <button type="submit" onClick={handleSubmit}>
                  Submit
                </button>
              </div>
            </div>
          </>
        );
      }}
    </Formik>
  );
};

export default DummySecond;
