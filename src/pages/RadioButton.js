import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";

const RadioButton = () => {
  const SignupSchema = Yup.object({
    gender: Yup.string().required("Required"),
  });

  return (
    <div className="mybtn ml-5">
      <Formik
        initialValues={{
          gender: "",
        }}
        // validations
        validationSchema={SignupSchema}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {(formik) => (
          <Form onSubmit={formik.handleSubmit}>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                id="male"
                type="radio"
                value="male"
                name="gender"
                onChange={formik.handleChange}
              />
              <label className="form-check-label" htmlFor="male">
                Male
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                id="female"
                type="radio"
                value="female"
                name="gender"
                onChange={formik.handleChange}
              />
              <label className="form-check-label" htmlFor="female">
                Female
              </label>
            </div>
            <br />
            {formik.errors.gender}
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default RadioButton;
