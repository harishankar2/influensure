import React from "react";

const SocialButton = (props) => {
  return (
    <>
      <button
        className="border border-red-700"
        onClick={this.props.triggerLogin}
        {...this.props}
      >
        {this.props.children}
      </button>
    </>
  );
};

export default SocialButton;
