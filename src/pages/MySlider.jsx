import React, { useState } from "react";
import Slider from "react-input-slider";
import CustomSlider from "../components/CustomSlider";

const MySlider = () => {
  const [state, setState] = useState({ x: 2000 });
  return (
    <div>
      <div>{"x: " + state.x}</div>
      <Slider
        axis="x"
        xstep={10}
        xmin={0}
        xmax={10000}
        x={state.x}
        onChange={({ x }) => setState({ x: parseFloat(x.toFixed(2)) })}
        styles={{
          root: {
            color: "#ff8896",
            height: 8,
          },
          thumb: {
            height: 24,
            width: 24,
            backgroundColor: "#201d5e",
            "&:focus, &:hover, &$active": {
              boxShadow: "inherit",
            },
          },
          active: {
            color: "#000",
          },
          valueLabel: {
            left: "calc(-50% + 7px)",
          },
          track: {
            height: 8,
            borderRadius: 4,
          },
          rail: {
            height: 8,
            borderRadius: 4,
            backgroundColor: "#e2e2e2",
          },
        }}
      />
      <CustomSlider />
    </div>
  );
};

export default MySlider;
