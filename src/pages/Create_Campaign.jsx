import React from "react";
import Campaign from "../components/Campaign";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";

const Create_Campaign = () => {
  return (
    <>
      <Navbar />
      <Campaign />
      <Footer />
    </>
  );
};

export default Create_Campaign;
