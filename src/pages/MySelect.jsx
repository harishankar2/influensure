import React from "react";
import Select from "react-select";
import { useFormik } from "formik";
import * as Yup from "yup";

const MySelect = () => {
  const initialValues = {
    country: [],
  };

  const onSubmit = (values) => {
    console.log(values);
  };

  const validationSchema = Yup.object({
    country: Yup.array()
      .nullable()
      .min(1, "Please select the field properly")
      .of(
        Yup.object().shape({
          label: Yup.string().required(),
          value: Yup.string().required(),
        })
      ),
  });

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
  });

  const countriesList = [
    { label: "Afghanistan", value: "AF" },
    { label: "India", value: "IND" },
    { label: "Bangladesh", value: "BAN" },
    { label: "Australia", value: "AUS" },
    { label: "Germany", value: "GER" },
  ];

  function onCountryChange(selectedCountry) {
    formik.setFieldValue("country", selectedCountry);
  }

  return (
    <div className="col-6 mx-auto mt-5">
      <div className="myfield">
        <form onSubmit={formik.handleSubmit}>
          <Select
            required
            id="country"
            name="country"
            placeholder={"Country"}
            onChange={(selectedOption) => {
              console.log("123");
              onCountryChange(selectedOption);
            }}
            options={countriesList}
          />
          {formik.errors.country}
          <button type="submit" className="btn btn-primary mt-4">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default MySelect;
