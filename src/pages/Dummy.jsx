import React from "react";
import DummyForm from "../components/DummyForm";
import "../components/dummy.css";

const Dummy = () => {
  return (
    <>
      <DummyForm />
    </>
  );
};

export default Dummy;
