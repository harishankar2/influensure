import React, { useContext } from "react";
import { UserContext } from "./UserContext";

const Index = () => {
  const { value, setValue } = useContext(UserContext);
  return (
    <div>
      <h1 className="text-3xl">Index</h1>
      <h1>{value}</h1>
      <button onClick={() => setValue("Heyyyyyyy")}>Change</button>
    </div>
  );
};

export default Index;
