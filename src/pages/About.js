import React, { useContext } from "react";
import { UserContext } from "./UserContext";

const About = () => {
  const { value, setValue } = useContext(UserContext);
  return (
    <div>
      <h1 className="text-3xl">About</h1>
      <h1>{value}</h1>
      <button onClick={() => setValue("Heyyyyyyy")}>Change</button>
    </div>
  );
};

export default About;
