import React, { useContext } from "react";
/* import SocialButton from "./SocialButton"; */
import { OldSocialLogin as SocialLogin } from "react-social-login";
import { navigate } from "@reach/router";
import { LoginContext } from "../components/LoginContext";

const Signin = () => {
  const { signIn, setSignIn } = useContext(LoginContext);

  const handleSocialLogin = (user, err) => {
    console.log(user);
    console.log(err);

    localStorage.setItem("login", JSON.stringify(user));
    navigate("/");

    if (err === null) {
      setSignIn(true);
      navigate("/");
    }
  };
  return (
    <div className=" bg-gray-bg h-screen mx-auto">
      <div className=" w-52 mx-auto my-5 py-1 px-6 rounded text-center ">
        <SocialLogin
          provider="facebook"
          appId="404001110863073"
          callback={handleSocialLogin}
        >
          <button className="text-blue-text rounded py-2 px-2 my-3 font-medium bg-pink-btn text-sm">
            Login with Facebook
          </button>
        </SocialLogin>
        <SocialLogin
          provider="google"
          appId="134112836698-97b0jsgqbql9eot3obt7c84cfd0gqd4i.apps.googleusercontent.com"
          callback={handleSocialLogin}
        >
          <button className="text-blue-text rounded py-2 px-2 my-3 font-medium bg-pink-btn text-sm">
            Login with Google
          </button>
        </SocialLogin>
      </div>
    </div>
  );
};

export default Signin;
