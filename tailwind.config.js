const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      ...colors,
      black: {
        ...colors.black,
        primary: "#000000",
      },
      gray: {
        ...colors.gray,
        placeholder: "#919191",
        bg: "#f5f7fc",
      },
      blue: {
        ...colors.blue,
        text: "#201d5e",
        border: "#8cc8ff",
        border_select: "#b6cbff",
      },
      pink: {
        ...colors.pink,
        btn: "#ff8896",
      },
      white: {
        ...colors.white,
        btn: "#ffffff",
        card_value: "#f5f6ff",
      },
    },
    height: {
      footer_img: {
        height: "20px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
